# Howto create RabbitMQ infra on the remote host with ANSIBLE #

If you consider to have larger AMQP infrastructure with RabbitMQ then 
definitely try to use Ansible as the infrastructure deployer, 
because Ansible's RabbitMQ modules are incredibly easy to use, let's show an example.

**Task:**

Create DIRECT exchange with name: **ansible_exchange** binded with two queues **myRemoteQueue1** 
and **myRemoteQueue2** with routing keys **key1** and **key2**.

**Solution:**

Let's create easy ansible role (create_infra) for the infrastructure deploy with **just** task directory:

**../roles/create_infra/tasks/main.yml**

	# Create exchange on remote host
	- rabbitmq_exchange:
   		name: ansible_exchange
   		type: direct
   		login_user: guest
   		login_password: guest
   		login_port: 15672

	# Create a queues on remote host
	- rabbitmq_queue:
    	name: "{{item}}"
    	login_user: guest
    	login_password: guest
    	login_host: localhost
    	login_port: 15672
  	with_items:
    	- myRemoteQueue1
    	- myRemoteQueue2

	- rabbitmq_binding:
    	name: ansible_exchange
    	destination: myRemoteQueue1
    	type: queue
    	routing_key: key1

	- rabbitmq_binding:
    	name: ansible_exchange
    	destination: myRemoteQueue2
    	type: queue
    	routing_key: key2

and launch this role with easy basic playbook:

**..infra.yml**

	- hosts: localhost
  		gather_facts: false
  		roles:
    		- create_infra

I believe that create_infra's task code is selfexplanatory, for more informations see the following:

http://docs.ansible.com/ansible/latest/rabbitmq_exchange_module.html
http://docs.ansible.com/ansible/latest/rabbitmq_binding_module.html
http://docs.ansible.com/ansible/latest/rabbitmq_queue_module.html

Now let's start the infra.yml playbook:

tomask79:rabbit_infra tomask79$ ansible-playbook infra.yml

	PLAY [localhost] ********************************************************************************************************************************************************************

	TASK [create_infra : rabbitmq_exchange] *********************************************************************************************************************************************
	changed: [localhost]

	TASK [create_infra : rabbitmq_queue] ************************************************************************************************************************************************
	ok: [localhost] => (item=myRemoteQueue1)
	ok: [localhost] => (item=myRemoteQueue2)

	TASK [create_infra : rabbitmq_binding] **********************************************************************************************************************************************
	changed: [localhost]

	TASK [create_infra : rabbitmq_binding] **********************************************************************************************************************************************
	changed: [localhost]

	PLAY RECAP **************************************************************************************************************************************************************************
	localhost                  : ok=4    changed=3    unreachable=0    failed=0   

and check that the playbook did what we wanted:

**exchange creation verification:**

	tomask79:sbin tomask79$ ./rabbitmqctl list_exchanges | grep "ansible_"
	ansible_exchange	direct

**queues creation verification:**

	tomask79:sbin tomask79$ ./rabbitmqctl list_queues | grep myRemoteQueue*
	myRemoteQueue1	0
	myRemoteQueue2	0

**binding creation verification:**

	tomask79:sbin tomask79$ ./rabbitmqctl list_bindings | grep ansible_
	ansible_exchange	exchange	myRemoteQueue1	queue	key1	[]
	ansible_exchange	exchange	myRemoteQueue2	queue	key2	[]

Guys, use and have fun with Ansible's AMQP modules!

regards

Tomas